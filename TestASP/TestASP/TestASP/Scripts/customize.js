﻿
function setdocumentheight() {
    var titleheight = $("#dvheader").height();
    var footerheight = $("#footer").height();
    var docheight = $(document).height();
    var totalheight = docheight - (titleheight + footerheight + 300);
    $("#content").height(totalheight);
}